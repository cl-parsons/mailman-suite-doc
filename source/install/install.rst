Installation Instructions
=========================

.. toctree::
    :caption: Quick setup guide
    :maxdepth: 1
    :hidden:

    docker
    distro
    virtualenv



Depending on your setup and experience administering a machine, you can choose
one of the following installations methods:

* :ref:`docker-install`

* Installing Mailman from distro packages:

  * :ref:`debian-install`

* :ref:`virtualenv-install`, recommended for Production

